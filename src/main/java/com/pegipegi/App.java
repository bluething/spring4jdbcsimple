package com.pegipegi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
        CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
        Customer customer = new Customer(1, "Habib",31);
        customerDAO.insert(customer);
    	
        Customer customer1 = customerDAO.findByCustomerId(1);
        System.out.println(customer1);
        
        context.close();
    }
}
