/**
 * 
 */
package com.pegipegi.customer.dao;

import com.pegipegi.customer.model.Customer;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public interface CustomerDAO {
	public void insert(Customer customer);
	public Customer findByCustomerId(int custId);
}
