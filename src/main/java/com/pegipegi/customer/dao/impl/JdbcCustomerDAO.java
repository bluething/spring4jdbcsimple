/**
 * 
 */
package com.pegipegi.customer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.customer.model.Customer;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class JdbcCustomerDAO implements CustomerDAO {

	private DataSource datasource;

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#insert(com.pegipegi.customer.model.Customer)
	 */
	@Override
	public void insert(Customer customer)
	{
		String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		Connection conn = null;

		try {

			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, customer.getCustId());
			ps.setString(2, customer.getName());
			ps.setInt(3, customer.getAge());
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if(conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#findByCustomerId(int)
	 */
	@Override
	public Customer findByCustomerId(int custId)
	{
		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";

		Connection conn = null;

		try {
			conn = datasource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, custId);
			Customer customer = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				customer = new Customer(
						rs.getInt("CUST_ID"),
						rs.getString("NAME"), 
						rs.getInt("Age")
						);
			}
			rs.close();
			ps.close();
			return customer;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
	}

	public DataSource getDatasource()
	{
		return datasource;
	}

	public void setDatasource(DataSource datasource)
	{
		this.datasource = datasource;
	}

}
